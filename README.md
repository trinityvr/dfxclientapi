# DFXClientAPI R Package Overview

The **DFXClientAPI** R package is available for analysts who want to integrate DiamondFX application 
data within their own data transformation pipeline from an R interface.  The R package's functions and output are documented below and permissioned at the account level.  

---

## Install and Load the R Package

The **DFXClientAPI** R package is hosted from our [public repo](https://bitbucket.org/trinityvr/dfxclientapi) and can be installed and loaded as follows:

``` r
devtools::install_bitbucket("trinityvr/dfxclientapi")
library(DFXClientAPI)
```

---

## Account Key

Every function call within the **DFXClientAPI** requires the account key as the first parameter. The account 
key is accessible in the [Sim Admin](https://portal.trinity-dfx.com/#/account.html) portal under the [Account](https://portal.trinity-dfx.com/account.html)
 page in the tab marked **"keys"**.
 
``` r
 key <- "your key goes here" 
```

---

##  Accessing Batting Simulation Data

All associated data generated from [Batting Simulation](sim_client/#simulation-mode) mode is accessible via a single R function call.  Listed 
 below are the functions, parameters, and sample output.
 
### Routes, Methods, and Parameters

A single R function call will return all Batting Simulation data associated with the account.
 
``` r
data <- getBattingSimData(key, start.date, end.date)
```

The following parameters can be passed into the function

| Parameter  | Datatype  | Format   | Required   | Description   |  
|---|---|---|---|---|
|*key*   |STRING   |N/A   |*✔*   |Your account key goes here   |   
|*start.date*   |DATETIME   |YYYY-MM-DD HH:MM:SS   |*✔*   |Starting date boundary for the data pull   |   
|*end.date*     |DATETIME   |YYYY-MM-DD HH:MM:SS   |*✔*   |Ending date boundary for the data pull     |   
  
 
 
### Sample Output

The function response is returned across multiple dataframes:
 
* *SwingData*
* *PitchData*
* *SessionData*
* *AtBatClientId*

### Batting Simulation Output Glossary

#### Swing Data

The **Batting Simulation** *SwingData* dataframe data is as follows:

| Parameter  | Datatype  | Format| Description   |  
|---|---|---|---|
|*AtBatId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the simulated at bat   |   
|*PitchId*   |STRING  |N/A  | Unique DiamondFX identifier for the pitch thrown   |   
|*SwingId*   |STRING  |N/A   |Unique DiamondFX identifier for the swing |   
|*Speed*   |STRING  |N/A   |Average ppeed of the swing in MPH   |   
|*SpeedMax*   |STRING  |N/A   |Max speed of the swing in MPH   |   
|*Type*   |STRING  |N/A  |Type of swing   |   
|*Subtype*   |STRING  |N/A  |Subtype of the swing   |   
|*MaxBallDistance*   |DOUBLE  |N/A  |Max distance of a given hit per swing, measured in ft   |   
|*MaxBallHeight*   |DOUBLE  |N/A  | Max height of a given hit per swing, measured in ft   |   
|*Power*   |DOUBLE  |N/A  | Max power per swing, measured in Watts   |   
|*TimeOfImpact*   |DOUBLE  |N/A  |Start of forward motion to the moment of impact, measured in seconds   |   
|*AngleOfImpact*   |DOUBLE  |N/A  |Angle of the bat’s path, at impact, relative to horizontal, measured in degrees   |   
|*CollisionQ*   |DOUBLE  |N/A  |Collision efficiency of the bat to ball   |   
|*CollisionOffsetX*   |DOUBLE  |N/A  |x offset for collision efficiency of the bat to ball   |   
|*BallExitSpeed*   |DOUBLE  |N/A  |Exit speed of the ball at impact per swing, measured in mph   |   
|*DeviceType*   |STRING  |N/A  |Type of input tracking device used   |   
|*MountId*   |INTEGER  |N/A  |DiamondFX unique identifier for type of mount used for bat tracking   |   

#### Pitch Data

The **Batting Simulation** *PitchData* dataframe data is as follows:

| Parameter  | Datatype  | Format| Description   |  
|---|---|---|---|
|*PfxId*   |INTEGER  |N/A   |PitchFX event ID for the pitch thrown   |   
|*PfxGameId*   |STRING  |N/A  | String identifier for the game where the original pitch was thrown   |   
|*Delivery*   |STRING  |N/A   |String indication of the delivery type     |   
|*Type*   |STRING  |N/A   |String indication of the pitch type   |   
|*Speed*   |DOUBLE  |N/A  |Speed of the pitch in MPH   |   

#### Session Data

The **Batting Simulation** *SessionData* dataframe data is as follows:

| Parameter  | Datatype  | Format | Description   |  
|---|---|---|---|
|*Id*   |INTEGER  |N/A   |Unique DiamondFX identifier for the session   |   
|*Start*   |DATETIME  |YYYY-MM-DD HH:MM:SS  | Datetime object for the start of the session   |   
|*Mode*   |STRING  |N/A   |String identifier for the simulation mode selected      |   
|*SimId*   |Integer  |N/A   |Unique DiamondFX identifier for the simulation    |   
|*DeviceType*   |DOUBLE  |N/A  |Type of input of input tracking device used   |   
|*MountId*   |DOUBLE  |N/A  |DiamondFX unique identifier for type of mount used for bat tracking  (batting simulation relevant only)   |   
|*Version*   |DOUBLE  |N/A  |Identifier for DiamondFX application version   |   

#### At Bat Data

The **Batting Simulation** *AtBatId* dataframe data is as follows:

| Parameter  | Datatype  | Format | Description   |  
|---|---|---|---|
|*Id*   |INTEGER  |N/A   |Unique DiamondFX identifier for the at bat   |   
|*Date*   |DATETIME  |YYYY-MM-DD HH:MM:SS  | Datetime object for the creation of the simulation   |   
|*SessionId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the simulation      |   
|*PitcherId*   |STRING  |N/A   |String BAM Identifier for the pitcher in the simulation    |   
|*BatterId*   |STRING  |N/A  |String name of the batter in the simulation   |   
|*TeamId*   |String  |N/A  |String name of the team assigned to the player in the organization   | 

---

##  Accessing Pitch Recognition Data

All associated data generated from [Pitch Recognition](sim_client/#simulation-mode) mode is accessible via a single R function call. Listed 
 below are the functions, parameters, and sample output.
 
### Routes, Methods, and Parameters

A single R function call will return all Pitch Recognition data associated with the account.
 
``` r
data <- getPitchRecogData(key, start.date, end.date)
```

The following parameters can be passed into the function

| Parameter  | Datatype  | Format   | Required   | Description   |  
|---|---|---|---|---|
|*key*   |STRING   |N/A   |*✔*   |Your account key goes here   |   
|*start.date*   |DATETIME   |YYYY-MM-DD HH:MM:SS   |*✔*   |Starting date boundary for the data pull   |   
|*end.date*     |DATETIME   |YYYY-MM-DD HH:MM:SS   |*✔*   |Ending date boundary for the data pull     |   
  
 
 
### Sample Output

The function response is returned across multiple dataframes:

* *CallData*
* *ExerciseInfo*
* *ExerciseData*
* *PitchData*
* *SessionData*
* *AtBatClientId*

### Pitch Recognition Output Glossary

#### Call Data

The **Pitch Recognition** *CallData* dataframe data is as follows:

| Parameter  | Datatype  | Format| Description   |  
|---|---|---|---|
|*PitchId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the pitch thrown   |   
|*Score*     |INTEGER   |N/A                |1 for correct calls, 0 for incorrect       | 
|*ReactionTime*     |FLOAT   |N/A   |Reaction time in milliseconds from the end of the pitch, a (-) reaction time indicates the pitch was called before the end of the end of the pitch     | 
|*TrackedTime*     |FLOAT   |N/A   |Time in milliseconds the ball is within player central cone of vision     |
 
#### Exercise Info Data

The **Pitch Recognition** *ExerciseInfo* dataframe data is as follows:

| Parameter  | Datatype  | Format| Description   |  
|---|---|---|---|
|*ConfigId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the exercise configuration   |   

The **Pitch Recognition** *ExerciseData* dataframe data is as follows:

| Parameter  | Datatype  | Format| Description   |  
|---|---|---|---|
|*AtBatId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the at bat   |   
|*PitchSpeedScalar*   |DOUBLE  |N/A   | Scalar applied to speed of the pitch thrown   |   
|*TrajectoryCutoff*   |INTEGER  |N/A   |Feet from home plate the trajectory was occluded   |   
|*PitchBreak*   |BOOLEAN  |N/A   |Indication of whether the break of the pitch was rendered   |   
|*PitchAvatar*   |BOOLEAN  |N/A  |Indication of whether the break of the pitch was rendered   |   

The **Pitch Recognition** *PitchData* dataframe data is as follows:

| Parameter  | Datatype  | Format| Description   |  
|---|---|---|---|
|*PfxId*   |INTEGER  |N/A   |PitchFX event ID for the pitch thrown   |   
|*PfxGameId*   |STRING  |N/A  | String identifier for the game where the original pitch was thrown   |   
|*Delivery*   |STRING  |N/A   |String indication of the delivery type     |   
|*Type*   |STRING  |N/A   |String indication of the pitch type   |   
|*Speed*   |DOUBLE  |N/A  |Speed of the pitch in MPH   |   

#### Session Data

The **Pitch Recognition** *SessionData* dataframe data is as follows:

| Parameter  | Datatype  | Format | Description   |  
|---|---|---|---|
|*Id*   |INTEGER  |N/A   |Unique DiamondFX identifier for the session   |   
|*Start*   |DATETIME  |YYYY-MM-DD HH:MM:SS  | Datetime object for the start of the session   |   
|*Mode*   |STRING  |N/A   |String identifier for the simulation mode selected      |   
|*SimId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the simulation    |   
|*DeviceType*   |DOUBLE  |N/A  |Type of input of input tracking device used   |   
|*MountId*   |DOUBLE  |N/A  |DiamondFX unique identifier for type of mount used for bat tracking (batting simulation relevant only)   |   
|*Version*   |DOUBLE  |N/A  |Identifier for DiamondFX application version   |   

#### At Bat Data

The **Pitch Recognition** *AtBatId* dataframe data is as follows:

| Parameter  | Datatype  | Format | Description   |  
|---|---|---|---|
|*Id*   |INTEGER  |N/A   |Unique DiamondFX identifier for the at bat   |   
|*Date*   |DATETIME  |YYYY-MM-DD HH:MM:SS  | Datetime object for the creation of the simulation   |   
|*SessionId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the simulation      |   
|*PitcherId*   |STRING  |N/A   |String BAM Identifier for the pitcher in the simulation    |   
|*BatterId*   |STRING  |N/A  |String name of the batter in the simulation   |   
|*TeamId*   |STRING  |N/A  |String name of the team assigned to the player in the organization   |   

---

